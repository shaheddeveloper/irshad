<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLongReservationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('long_reservations', function(Blueprint $table)
		{
			$table->foreign('reservation_id', 'FK_LONGRES')->references('id')->on('reservations')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('room_id', 'FK_ROOMRES')->references('id')->on('rooms')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('long_reservations', function(Blueprint $table)
		{
			$table->dropForeign('FK_LONGRES');
			$table->dropForeign('FK_ROOMRES');
		});
	}

}

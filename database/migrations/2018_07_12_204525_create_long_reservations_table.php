<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLongReservationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('long_reservations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('day_of_the_week');
			$table->integer('floor_id')->nullable()->index('FK_ROOMRES_idx');
			$table->integer('reservation_id')->index('FK_LONGRES_idx');
			$table->time('from_time');
			$table->time('to_time');
			$table->string('event_name')->nullable();
			$table->dateTime('from_date');
			$table->dateTime('to_date');
			$table->integer('room_id')->nullable()->index('FK_ROOMRES_idx1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('long_reservations');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemporaryReservationPlaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temporary_reservation_place', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('temporary_reservation_id')->index('FK_RESERVATIONPLACE_idx');
			$table->integer('room_id')->index('FK_ROOMRESERVATIONPLACE_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temporary_reservation_place');
	}

}
